﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
     public Transform alvo;
     public float velocidadeRelativa;
     public float posicaoAntX;

     void Start() {
        if(velocidadeRelativa < 1) {
            velocidadeRelativa = 1;
        }

        alvo = GameObject.FindGameObjectWithTag("gordo").transform;
        posicaoAntX = alvo.position.x;
    }

    void EfeitoParallax() {

        transform.Translate((alvo.position.x - posicaoAntX) / velocidadeRelativa,0,0);

        posicaoAntX = alvo.position.x;
    }

    void Update() {
        EfeitoParallax();
    }
    
}

    /*
    private float length, startpos;
    public GameObject cam;
    public float parallaxExffect;

    void Start() {
        startpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update() {
        float temp = (cam.transform.position.x * (1 - parallaxExffect));
        float dist = (cam.transform.position.x * parallaxExffect);

        transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);

        if (temp > startpos + length){
            startpos += length;
        }

        if (temp < startpos - length) {
            startpos -= length;
        }
    }
    */

    /*
    [SerializeField] private Vector2 parallaxExffectMultiplier;
    private Transform cameraTransform;
    private Vector3 lastCameraPosition;
    private float textureUnitSizeX;
    private float textureUnitSizeY;

    void Start() {
        cameraTransform = Camera.main.transform;
        lastCameraPosition = cameraTransform.position;
        Sprite sprite = GetComponent<SpriteRenderer>().sprite;
        Texture2D texture = sprite.texture;
        textureUnitSizeX = texture.width / sprite.pixelsPerUnit;
        textureUnitSizeY = texture.height / sprite.pixelsPerUnit;
    }

    void LateUpdate() {

        Vector3 deltaMovement = cameraTransform.position - lastCameraPosition;
        //float parallaxExffectMultiplier = .5f;
        transform.position += new Vector3(deltaMovement.x * parallaxExffectMultiplier.x, deltaMovement.y * parallaxExffectMultiplier.y);
        lastCameraPosition = cameraTransform.position;

        if (Mathf.Abs(cameraTransform.position.x - transform.position.x) >= textureUnitSizeX) {
            float offsetPositionX = (cameraTransform.position.x - transform.position.x) % textureUnitSizeX;
            transform.position = new Vector3(cameraTransform.position.x + offsetPositionX, transform.position.y);
        }

        if (Mathf.Abs(cameraTransform.position.y - transform.position.y) >= textureUnitSizeY) {
            float offsetPositionY = (cameraTransform.position.y - transform.position.y) % textureUnitSizeY;
            transform.position = new Vector3(transform.position.y, cameraTransform.position.y + offsetPositionY);
        }

    }
    */